CREATE DATABASE mahasiswa;
USE mahasiswa;

CREATE TABLE data_mahasiswa (
    id int(100) NOT NULL PRIMARY KEY, 
    npm CHAR(12) NOT NULL, 
    nama VARCHAR(50) NOT NULL,
    jurusan VARCHAR(50) NOT NULL
);

INSERT INTO datamhs VALUES
    ('1','14081018001', 'Muhammad ', 'Teknik Informatika'),
    ('2','14081018002', 'Dedi', 'Teknik Informatika'),
    ('3','14081018003', 'Erik', 'Teknik Informatika'),
    ('4','14081018004', 'Dodi', 'Teknik Informatika'),
    ('5','14081018005', 'Dudung', 'Teknik Informatika');
