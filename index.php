<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Data Mahasiswa</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="js.js"></script>
</head>
<body>
    <div>
        <h1><center>Data Mahasiswa</center></h1>
    </div>
    <table class=" table table-bordered">
        <thead>
            <tr>
                <th style="text-align: center">Nama</th>
                <th style="text-align: center">NPM</th>
                <th style="text-align: center">Jurusan</th>
            </tr>
        </thead>
        <tbody id="datamhs">
        </tbody>
    </table>
    <center><button type="button" class="btn btn-primary" id="showMore">Tampilkan lebih banyak...</button></center>
</body>
</html>
